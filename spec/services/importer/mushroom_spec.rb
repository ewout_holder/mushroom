require 'rails_helper'

RSpec.describe Importer::Mushroom do
  context 'general' do
    it 'can be started' do
      expect(Importer::Mushroom).to respond_to(:start)
    end

    it 'needs to be started with a file' do
      expect { Importer::Mushroom.start }.to raise_error(MissingImportFileException)
    end
  end

  context 'start' do
    before do
      file_name = [fixture_path, 'mushroom_metadata_10.txt'].join('/')
      Importer::Mushroom.start(file_name)
    end

    it 'will create a mushroom-item in the database for each line in the import file' do
      expect(Mushroom.all.count).to eq(10)
    end

    it 'stores the generated name in the mushroom-table' do
      expect(Mushroom.first.name).to eq('pxsntpfcnkeesswwpwopksu')
    end
  end

  context 'attributes' do
    it 'will create attributes' do
      Importer::Mushroom.load_attributes

      expect(Attribute.all.count).to eq(23)
      expect(Attribute.find_by(name: 'edible').allowed_values).to eq('edible,poisonous')
    end
  end

  context 'convert import line to mushroom' do
    let(:parsed_mushroom) { build_parshed_mushroom_from_import_line }

    it 'returns an object of type ParsedMushroom' do
      expect(parsed_mushroom).to be_kind_of(ParsedMushroom)
    end

    it 'generate a name for the parsed mushroom' do
      expect(parsed_mushroom.name).to eq('pxsntpfcnkeesswwpwopksu')
    end

    it 'has an object with all fields correctly populated' do
      expect(parsed_mushroom.edible).to eq('poisonous')
      expect(parsed_mushroom.cap_shape).to eq('convex')
      expect(parsed_mushroom.cap_surface).to eq('smooth')
      expect(parsed_mushroom.cap_color).to eq('brown')
      expect(parsed_mushroom.bruises).to eq(true)
      expect(parsed_mushroom.odor).to eq('pungent')
      expect(parsed_mushroom.gill_attachment).to eq('free')
      expect(parsed_mushroom.gill_spacing).to eq('close')
      expect(parsed_mushroom.gill_size).to eq('narrow')
      expect(parsed_mushroom.gill_color).to eq('black')
      expect(parsed_mushroom.stalk_shape).to eq('enlarging')
      expect(parsed_mushroom.stalk_root).to eq('equal')
      expect(parsed_mushroom.stalk_surface_above_ring).to eq('smooth')
      expect(parsed_mushroom.stalk_surface_below_ring).to eq('smooth')
      expect(parsed_mushroom.stalk_color_above_ring).to eq('white')
      expect(parsed_mushroom.stalk_color_below_ring).to eq('white')
      expect(parsed_mushroom.veil_type).to eq('partial')
      expect(parsed_mushroom.veil_color).to eq('white')
      expect(parsed_mushroom.ring_number).to eq('one')
      expect(parsed_mushroom.ring_type).to eq('pendant')
      expect(parsed_mushroom.spore_print_color).to eq('black')
      expect(parsed_mushroom.population).to eq('scattered')
      expect(parsed_mushroom.habitat).to eq('urban')
    end
  end

  def build_parshed_mushroom_from_import_line
    file_name = [
      fixture_path(),
      'mushroom_metadata.txt'
    ].join('/')

    line = File.read(file_name).split("\n").first

    Importer::Mushroom.convert(line)
  end
end
