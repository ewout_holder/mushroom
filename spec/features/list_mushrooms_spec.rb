require 'rails_helper'

feature 'Index', type: :feature do
  context 'list of mushrooms' do
    before do
      load_test_mushrooms
    end

    it 'all when unfiltered' do
      visit root_path

      expect(page).to have_selector('#mushrooms_index')
      expect(page).to have_selector('table#mushrooms')

      table = page.find('table#mushrooms')
      expect(table).to have_content('pxsntpfcnkeesswwpwopksu') # generated name of the first line in the import-file
      expect(table).to have_content('exsytafcbkecsswwpwopnng') # generated name of the first edible mushroom
    end
  end

  context 'filtering' do
    it 'shows the filter-options' do
      Importer::Mushroom.load_attributes

      visit root_path

      expect(page).to have_selector('form#filtering')
    end

    it 'can filter the mushrooms on selector "edible"' do
      load_test_mushrooms

      visit root_path

      select('poisonous', from: 'filters[edible]')
      click_button('Apply filters')

      table = page.find('table#mushrooms')
      expect(table).to have_content('pxsntpfcnkeesswwpwopksu') # generated name of the first line in the import-file
      expect(table).to_not have_content('exsytafcbkecsswwpwopnng') # generated name of the first edible mushroom
    end

    it 'can be reset' do
      load_test_mushrooms

      visit root_path

      select('poisonous', from: 'filters[edible]')
      click_button('Apply filters')

      click_link 'Reset filters'

      table = page.find('table#mushrooms')
      expect(table).to have_content('pxsntpfcnkeesswwpwopksu')
      expect(table).to have_content('exsytafcbkecsswwpwopnng')
    end
  end

  def load_test_mushrooms
    file_name = [fixture_path, 'mushroom_metadata_10.txt'].join('/')

    Importer::Mushroom.start(file_name)
  end
end
