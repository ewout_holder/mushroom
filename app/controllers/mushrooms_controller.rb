class MushroomsController < ApplicationController
  def index
    @mushrooms =
      if params['filters']
        Mushroom.set_filters(params.require(:filters).permit!.to_hash)
      else
        Mushroom.all
      end

    @filters =
      Attribute.all.inject({}) do |acc, attribute|
        acc[attribute.name] = (attribute.allowed_values || []).split(',')
        acc
      end
  end
end
