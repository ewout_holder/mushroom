class ParsedMushroom < ::Objection::Base
  requires(
    :name,
    :edible,
    :cap_shape, :cap_surface, :cap_color,
    :bruises, :odor,
    :gill_attachment, :gill_spacing, :gill_size, :gill_color,
    :stalk_shape, :stalk_root,
    :stalk_surface_above_ring, :stalk_surface_below_ring,
    :stalk_color_above_ring, :stalk_color_below_ring,
    :veil_type, :veil_color,
    :ring_number, :ring_type,
    :spore_print_color, :population, :habitat
  )

  def to_model
    mushroom = Mushroom.new(name: self.name)

    attribute_list = to_hash
    attribute_list.delete(:name)

    attribute_list.each do |key, value|
      attribute = Attribute.find_or_initialize_by(name: key)
      mushroom.mushroom_attributes.build(attribute_item: attribute, value: value)
    end

    mushroom
  end
end
