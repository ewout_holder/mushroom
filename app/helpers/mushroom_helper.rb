module MushroomHelper
  def selected_value(attribute_name)
    return nil unless params['filters']

    params['filters'][attribute_name]
  end
end
