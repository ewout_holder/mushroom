require 'rails_helper'

RSpec.describe ParsedMushroom do
  context 'to_model' do
    let(:parsed_mushroom) { build_parshed_mushroom }
    let(:mushroom) { parsed_mushroom.to_model }

    it 'returns an object of kind Mushroom' do
      expect(mushroom).to be_kind_of(Mushroom)
    end

    it 'returns a storable object' do
      expect(mushroom.valid?).to eq(true)
    end

    context 'saving' do
      before do
        mushroom.save
      end

      it 'stores the name in the mushroom-model' do
        expect(Mushroom.first.name).to eq('test mushroom-name')
      end
      it 'stores all mushroom-attributes for the saved mushroom' do
        expect(MushroomAttribute.all.count).to eq(23)
      end
      it 'links all stored mushroom-attributes to the current mushroom' do
        expect(mushroom.mushroom_attributes.count).to eq(23)
      end
      it 'stores the mushroom-attributes values' do
        attr_edible = Attribute.find_by(name: 'edible')
        attr_edible_value =
          mushroom.mushroom_attributes.detect do |mushroom_attribute|
            mushroom_attribute.attribute_item == attr_edible
          end.value

        expect(attr_edible_value).to eq('value-edible')
      end
    end
  end

  def build_parshed_mushroom
    hash = {
      name:                     'test mushroom-name',
      edible:                   'value-edible',
      cap_shape:                'cap_shape',
      cap_surface:              'cap_surface',
      cap_color:                'cap_color',
      bruises:                  'bruises',
      odor:                     'odor',
      gill_attachment:          'gill_attachment',
      gill_spacing:             'gill_spacing',
      gill_size:                'gill_size',
      gill_color:               'gill_color',
      stalk_shape:              'stalk_shape',
      stalk_root:               'stalk_root',
      stalk_surface_above_ring: 'stalk_surface_above_ring',
      stalk_surface_below_ring: 'stalk_surface_below_ring',
      stalk_color_above_ring:   'stalk_color_above_ring',
      stalk_color_below_ring:   'stalk_color_below_ring',
      veil_type:                'veil_type',
      veil_color:               'veil_color',
      ring_number:              'ring_number',
      ring_type:                'ring_type',
      spore_print_color:        'spore_print_color',
      population:               'population',
      habitat:                  'habitat'
    }

    ParsedMushroom.new(hash)
  end
end
