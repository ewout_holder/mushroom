class CreateMushroomAttributes < ActiveRecord::Migration[5.2]
  def change
    create_table :mushroom_attributes do |t|
      t.references :mushroom, foreign_key: true
      t.references :attribute_item, foreign_key: true
      t.string :value

      t.timestamps
    end
  end
end
