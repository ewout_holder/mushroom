module Importer
  module Mushroom
    class << self
      def start(file = nil)
        file.nil? && raise(MissingImportFileException)

        load_attributes

        lines = File.read(file).strip.split("\n")
        lines.each do |line|
          parsed_mushroom = self.convert(line)
          mushroom = parsed_mushroom.to_model
          mushroom.save
        end
      end

      def convert(import_line)
        attributes = import_line.split(',')

        name = import_line.gsub(',', '')

        hash_mushroom =
          convert_functions.inject({name: name}) do |acc, function|
            acc.merge(self.send(function, attributes))
          end

        ParsedMushroom.new(hash_mushroom)
      end

      def load_attributes
        Attribute.create(name: 'edible',                   allowed_values: 'edible,poisonous')
        Attribute.create(name: 'cap_shape',                allowed_values: 'bell,conical,convex,flat,knobbed,sunken')
        Attribute.create(name: 'cap_surface',              allowed_values: 'fibrous,grooves,scaly,smooth')
        Attribute.create(name: 'cap_color',                allowed_values: 'brown,buff,cinnamon,gray,green,pink,purple,red,white,yellow')
        Attribute.create(name: 'bruises',                  allowed_values: 'true,false')
        Attribute.create(name: 'odor',                     allowed_values: 'almond,anise,creosote,fishy,foul,musty,none,pungent,spicy')
        Attribute.create(name: 'gill_attachment',          allowed_values: 'attached,descending,free,notched')
        Attribute.create(name: 'gill_spacing',             allowed_values: 'close,crowded,distant')
        Attribute.create(name: 'gill_size',                allowed_values: 'broad,narrow')
        Attribute.create(name: 'gill_color',               allowed_values: 'black,brown,buff,chocolate,gray,green,orange,pink,purple,red,white,yellow')
        Attribute.create(name: 'stalk_shape',              allowed_values: 'enlarging,tapering')
        Attribute.create(name: 'stalk_root',               allowed_values: 'bulbous,club,cup,equal,rhizomorphs,rooted,missing')
        Attribute.create(name: 'stalk_surface_above_ring', allowed_values: 'fibrous,scaly,silky,smooth')
        Attribute.create(name: 'stalk_surface_below_ring', allowed_values: 'fibrous,scaly,silky,smooth')
        Attribute.create(name: 'stalk_color_above_ring',   allowed_values: 'brown,buff,cinnamon,gray,orange,pink,red,white,yellow')
        Attribute.create(name: 'stalk_color_below_ring',   allowed_values: 'brown,buff,cinnamon,gray,orange,pink,red,white,yellow')
        Attribute.create(name: 'veil_type',                allowed_values: 'partial,universal')
        Attribute.create(name: 'veil_color',               allowed_values: 'brown,orange,white,yellow')
        Attribute.create(name: 'ring_number',              allowed_values: 'none,one,two')
        Attribute.create(name: 'ring_type',                allowed_values: 'cobwebby,evanescent,flaring,large,none,pendant,sheathing,zone')
        Attribute.create(name: 'spore_print_color',        allowed_values: 'black,brown,buff,chocolate,green,orange,purple,white,yellow')
        Attribute.create(name: 'population',               allowed_values: 'abundant,clustered,numerous,scattered,several,solitary')
        Attribute.create(name: 'habitat',                  allowed_values: 'grasses,leaves,meadows,paths,urban,waste,woods')
      end

      private

        def convert_functions
          %i{
            convert_edible
            convert_cap_shape
            convert_cap_surface
            convert_cap_color
            convert_bruises
            convert_odor
            convert_gill_attachment
            convert_gill_spacing
            convert_gill_size
            convert_gill_color
            convert_stalk_shape
            convert_stalk_root
            convert_stalk_surface_above_ring
            convert_stalk_surface_below_ring
            convert_stalk_color_above_ring
            convert_stalk_color_below_ring
            convert_veil_type
            convert_veil_color
            convert_ring_number
            convert_ring_type
            convert_spore_print_color
            convert_population
            convert_habitat
          }
        end

        def convert_edible(attributes)
          {edible: (attributes[0] == 'p') ? 'poisonous' : 'edible'}
        end

        def convert_cap_shape(attributes)
          value = {
            'b' => 'bell',
            'c' => 'conical',
            'x' => 'convex',
            'f' => 'flat',
            'k' => 'knobbed',
            's' => 'sunken'
          }[attributes[1]]

          {cap_shape: value}
        end

        def convert_cap_surface(attributes)
          value = {
            'f' => 'fibrous',
            'g' => 'grooves',
            'y' => 'scaly',
            's' => 'smooth'
          }[attributes[2]]

          {cap_surface: value}
        end

        def convert_cap_color(attributes)
          value = {
            'n' => 'brown',
            'b' => 'buff',
            'c' => 'cinnamon',
            'g' => 'gray',
            'r' => 'green',
            'p' => 'pink',
            'u' => 'purple',
            'e' => 'red',
            'w' => 'white',
            'y' => 'yellow'
          }[attributes[3]]

          {cap_color: value}
        end

        def convert_bruises(attributes)
          {bruises: (attributes[4] == 't') ? true : false}
        end

        def convert_odor(attributes)
          value = {
            'a' => 'almond',
            'l' => 'anise',
            'c' => 'creosote',
            'y' => 'fishy',
            'f' => 'foul',
            'm' => 'musty',
            'n' => 'none',
            'p' => 'pungent',
            's' => 'spicy'
          }[attributes[5]]

          {odor: value}
        end

        def convert_gill_attachment(attributes)
          value = {
            'a' => 'attached',
            'd' => 'descending',
            'f' => 'free',
            'n' => 'notched'
          }[attributes[6]]

          {gill_attachment: value}
        end

        def convert_gill_spacing(attributes)
          value = {
            'c' => 'close',
            'w' => 'crowded',
            'd' => 'distant'
          }[attributes[7]]

          {gill_spacing: value}
        end

        def convert_gill_size(attributes)
          value = {
            'b' => 'broad',
            'n' => 'narrow'
          }[attributes[8]]

          {gill_size: value}
        end

        def convert_gill_color(attributes)
          value = {
            'k' => 'black',
            'n' => 'brown',
            'b' => 'buff',
            'h' => 'chocolate',
            'g' => 'gray',
            'r' => 'green',
            'o' => 'orange',
            'p' => 'pink',
            'u' => 'purple',
            'e' => 'red',
            'w' => 'white',
            'y' => 'yellow'
          }[attributes[9]]

          {gill_color: value}
        end

        def convert_stalk_shape(attributes)
          value = {
            'e' => 'enlarging',
            't' => 'tapering'
          }[attributes[10]]

          {stalk_shape: value}
        end

        def convert_stalk_root(attributes)
          value = {
            'b' => 'bulbous',
            'c' => 'club',
            'u' => 'cup',
            'e' => 'equal',
            'z' => 'rhizomorphs',
            'r' => 'rooted',
            '?' => 'missing'
          }[attributes[11]]

          {stalk_root: value}
        end

        def convert_stalk_surface_above_ring(attributes)
          value = {
            'f' => 'fibrous',
            'y' => 'scaly',
            'k' => 'silky',
            's' => 'smooth'
          }[attributes[12]]

          {stalk_surface_above_ring: value}
        end

        def convert_stalk_surface_below_ring(attributes)
          value = {
            'f' => 'fibrous',
            'y' => 'scaly',
            'k' => 'silky',
            's' => 'smooth'
          }[attributes[13]]

          {stalk_surface_below_ring: value}
        end

        def convert_stalk_color_above_ring(attributes)
          value = {
            'n' => 'brown',
            'b' => 'buff',
            'c' => 'cinnamon',
            'g' => 'gray',
            'o' => 'orange',
            'p' => 'pink',
            'e' => 'red',
            'w' => 'white',
            'y' => 'yellow'
          }[attributes[14]]

          {stalk_color_above_ring: value}
        end

        def convert_stalk_color_below_ring(attributes)
          value = {
            'n' => 'brown',
            'b' => 'buff',
            'c' => 'cinnamon',
            'g' => 'gray',
            'o' => 'orange',
            'p' => 'pink',
            'e' => 'red',
            'w' => 'white',
            'y' => 'yellow'
          }[attributes[15]]

          {stalk_color_below_ring: value}
        end

        def convert_veil_type(attributes)
          value = {
            'p' => 'partial',
            'u' => 'universal'
          }[attributes[16]]

          {veil_type: value}
        end

        def convert_veil_color(attributes)
          value = {
            'n' => 'brown',
            'o' => 'orange',
            'w' => 'white',
            'y' => 'yellow'
          }[attributes[17]]

          {veil_color: value}
        end

        def convert_ring_number(attributes)
          value = {
            'n' => 'none',
            'o' => 'one',
            't' => 'two'
          }[attributes[18]]

          {ring_number: value}
        end

        def convert_ring_type(attributes)
          value = {
            'c' => 'cobwebby',
            'e' => 'evanescent',
            'f' => 'flaring',
            'l' => 'large',
            'n' => 'none',
            'p' => 'pendant',
            's' => 'sheathing',
            'z' => 'zone'
          }[attributes[19]]

          {ring_type: value}
        end

        def convert_spore_print_color(attributes)
          value = {
            'k' => 'black',
            'n' => 'brown',
            'b' => 'buff',
            'h' => 'chocolate',
            'r' => 'green',
            'o' => 'orange',
            'u' => 'purple',
            'w' => 'white',
            'y' => 'yellow'
          }[attributes[20]]

          {spore_print_color: value}
        end

        def convert_population(attributes)
          value = {
            'a' => 'abundant',
            'c' => 'clustered',
            'n' => 'numerous',
            's' => 'scattered',
            'v' => 'several',
            'y' => 'solitary'
          }[attributes[21]]

          {population: value}
        end

        def convert_habitat(attributes)
          value = {
            'g' => 'grasses',
            'l' => 'leaves',
            'm' => 'meadows',
            'p' => 'paths',
            'u' => 'urban',
            'w' => 'waste',
            'd' => 'woods',
          }[attributes[22]]

          {habitat: value}
        end
    end
  end
end
