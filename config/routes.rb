Rails.application.routes.draw do
  resources :mushrooms, only: [:index]

  root 'mushrooms#index'
end
