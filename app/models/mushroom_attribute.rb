class MushroomAttribute < ApplicationRecord
  belongs_to :mushroom
  belongs_to :attribute_item, class_name: :Attribute
end
