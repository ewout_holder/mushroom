# README

## Import dataset

The dataset is downloaded from https://archive.ics.uci.edu/ml/datasets/Mushroom to directory /docs
The data can be loaded via an import-script. In a rails console, give: 

    Importer::Mushroom.start('docs/adcombi/agaricus-lepiota.data')

All data is stored in tables with a key-value setup. 

## Running
Just go to the directory, start the server with `rails s` and in the browser surf to url 'localhost:3000'

## ERD
I didn't feel like creating a mushroom-table with 24 columns (name + 23 attributes). Instead, I decided to build a key-value setup: there is a mushroom-table, an attribute-table, and a link-table wich combines the ids of both these tables. The link-table also holds the value per combination of mushroom / attribute.
Did turn out to give a challenge when filtering the list of mushrooms, because we can only filter on a value for the rows where the attribute-id is a certain-value.
This can be done by joining with the link-table multiple times, but that requires an alias per join.

All this is outside the default joining of ActiveRecord, hence the custom sql in the join-statements.

## Weird thing

The datafile only contains the values from the attributes, but not the names of the mushrooms.
In order to overcome this problem, the names are generated.
The importer takes the line from the data-file, removes all commas, and the resulting string becomes the name of the mushroom.
This is quite handy for validating wether the code works; just select 'edible = poisonous'. The resulting list should only show mushroom-names starting with an 'p'.
This is because the edible attribute is the first attribute, and the 'p' stands for poisonous.
Likewise, selecting 'habitat = grasses' will only show mushrooms ending with a 'g'.

# ToDo
Things that could have been done:
- Refresh the list of mushrooms as soon as a filter has been updated
- Paginating the list via the gem 'kaminari'
- Caching the attributes in a Singleton
