# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_24_163347) do

  create_table "attributes", force: :cascade do |t|
    t.string "name"
    t.string "allowed_values"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "mushroom_attributes", force: :cascade do |t|
    t.integer "mushroom_id"
    t.integer "attribute_item_id"
    t.string "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attribute_id"], name: "index_mushroom_attributes_on_attribute_id"
    t.index ["mushroom_id"], name: "index_mushroom_attributes_on_mushroom_id"
  end

  create_table "mushrooms", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
