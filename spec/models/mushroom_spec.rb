require 'rails_helper'

RSpec.describe 'mushroom' do
  context 'set filters' do
    it 'builds a relation-object for the filters' do
      expected_relation = 'SELECT "mushrooms".* FROM "mushrooms" INNER JOIN mushroom_attributes as join_1 on join_1.mushroom_id = mushrooms.id INNER JOIN mushroom_attributes as join_2 on join_2.mushroom_id = mushrooms.id WHERE "join_1"."attribute_item_id" = 1 AND "join_1"."value" = \'dummy-value\' AND "join_2"."attribute_item_id" = 2 AND "join_2"."value" = \'dummy-again\''

      Attribute.create(name: 'filter-1')
      Attribute.create(name: 'filter-2')
      relation = Mushroom.set_filters('filter-1' => 'dummy-value', 'filter-2' => 'dummy-again')

      expect(relation.to_sql).to eq(expected_relation)
    end
  end
end
