class Mushroom < ApplicationRecord
  has_many :mushroom_attributes
  has_many :attribute_items, through: :mushroom_attributes, table_name: 'attributes'

  def self.set_filters(filters)
    relation = Mushroom.all

    active_filters = filters.select { |_key, value| value.present?}

    join_index = 0
    active_filters.inject(Mushroom.all) do |relation, (attribute_name, value)|
      join_name = "join_#{join_index += 1}"
      attribute = Attribute.find_by!(name: attribute_name)
      relation
        .joins("INNER JOIN mushroom_attributes as #{join_name} on #{join_name}.mushroom_id = mushrooms.id")
        .where(join_name => {attribute_item_id: attribute.id, value: value})
    end
  end
end
